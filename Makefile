NAME=BucketService
VERSION=1.0.0

.PHONY: build
build:
	@go build -o build/$(NAME)

.PHONY: run
run: build
	@./build/$(NAME) -e development

.PHONY: run-prod
run-prod: build
	@./build/$(NAME) -e production

.PHONY: clean
clean:
	@rm -f build/$(NAME)

.PHONY: install
deps-save:
	@go install

.PHONY: test
test:
	@go test -v ./tests/*