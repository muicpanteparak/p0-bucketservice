package main

import (
	"bucketservice/db"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

func main(){

	router := gin.Default()
	v1 := router.Group("")
	{
		v1.POST("/", home)
	}
	router.Run()
}

func home(c *gin.Context)  {
	fmt.Println("This is Home")
	db.Init()
	c.JSON(http.StatusCreated, gin.H{"status": http.StatusCreated, "message": "This is home"})
}